In this section, the results are displayed and discussed.
%
\subsection{Radiation, Humidity and Stability}
%
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{img/radiation}
	\caption{Measured radiation components and expected incoming short wave radiation $K\downarrow_{theo}$.}
	\label{fig:radiation}
\end{figure}
%
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{img/RH}
	\caption{Relative humidity, H$_2$O mixing ratio and temperature at Mistral, and estimated start and end times of the rain event (from section \ref{sec:weather}).}
	\label{fig:RH}
\end{figure}
%
The radiation components measured by the radiometer are displayed in figure \ref{fig:radiation} and are compared to the expectations from section \ref{sec:fluxes}:\\
$-K\downarrow$ is very variable - apparently, sunny and cloudy moments alternated.
This is plausible because the sky was partly cloud covered.
The peaks exceed $-K\downarrow_{theo}$ by up to \SI{40}{\percent}.
It is assumed that this is due to cloud enhancement, but reflection at the met mast or the large error of the radiometer could also play a role.\\
$\sfrac{K\uparrow}{-K\downarrow}$ varies between $0.20$ and $0.27$.
This roughly meets the expectation from equation \ref{eq:albedo}.\\
$I\uparrow$ lies within the expected range (equation \ref{eq:blackbody}) and varies little, as expected.\\
$-I\downarrow$ varies a bit, most probably due to changing cloud cover.
It is always lower than $I\uparrow$, as expected.
Around 3 p.m., $-K\downarrow$ is lowest and $-I\downarrow$ is highest - this indicates high cloud cover, which coincides with the fact that it was raining at that time.
\\\\
Figure \ref{fig:RH} shows the relative humidity, H$_2$O mixing ratio and temperature.
It is expected that a rain event goes along with a decrease in temperature.
Therefore it is assumed that the rain event acually started when the temperature started to decrease, which is around 2:50 - later than the documented start of the rain event.
The H$_2$O mixing ratio increases slightly during and a bit faster after the rain event, until 3:35, when it drops rapidly.
What actually influences the H$_2$O mixing ratio and temperature at the met mast are the weather conditions and ground humidity not so much at the met mast, but rather upwind of the met mast, where the air comes from.
When air passes through rain, its H$_2$O mixing ratio increases.
The same applies when it passes over humid ground, especially when it is sunny.
Therefore, a possible explanation for the course of the H$_2$O mixing ratio is as follows:
During the rain event, the wind brought air from places where it had not rained yet.
Therefore, it was still dry.
After the rain event, the wind brought air that had passed through rain or over humid ground on its way to the measurement location, and was therefore humid.
The air coming to the met mast after 3:35 had not passed through rain or over humid ground and was therefore dry.
Maybe the wind direction had changed.
\\\\
As shown in section \ref{sec:errforest}, neutral stability conditions were given.
This can be due to the rain before and during the measurement period, which cooled the ground.

\subsection{Sensible and Latent Heat Fluxes of Mistral and Caurus}
%
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{img/Q_H}
	\caption{Sensible heat flux $Q_H$.}
	\label{fig:Q_H}
\end{figure}
%
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{img/Q_E}
	\caption{Latent heat flux $Q_E$.}
	\label{fig:Q_E}
\end{figure}
%
Figures \ref{fig:Q_H} and \ref{fig:Q_E} show the sensible and latent heat fluxes of Mistral and Caurus.
In the Prandtl layer, Caurus' values would be expected to be less than \SI{10}{\percent} lower than Mistral's.
In fact, they are considerably larger in most cases, not only between 2 and 3 p.m..
Therefore, this cannot be explained by the shaking of the met mast.
The observation is no surprise because in section \ref{sec:errforest}, it was shown that the forest influences the flow at the met mast such that the conditions at Caurus are different from the ones at Mistral.\\
The relation between the fluxes at Mistral and at Caurus cannot be interpreted further because too little is known about the flow conditions downwind of the forest.
%
%
\subsection{Heat Flux Balance}
%
\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{img/fluxes_Mistral}
\caption{Surface heat fluxes.}
\label{fig:fluxes}
\end{figure}

\begin{table}
	\centering
		\caption{Surface heat fluxes: Fraction of ground flux of radiation, and Bowen ratio.}
	\begin{tabular}{|l||r|r|}
	\hline
	time interval & $\frac{Q_G}{-Q_S}$ & $\frac{Q_H}{Q_E}$ \\\hline \hline
	2-2:30 & $0.30$ & \SI{0.33}{} \\\hline
	2:30-3 & $0.25$ & $0.09$ \\\hline
	3-3:30 & $0.46$ & $-0.01$ \\\hline
	3:30-4 & $0.13$ & $0.07$ \\\hline
	\end{tabular}

	\label{tab:groundbowen}
\end{table}
%
The heat fluxes at the surface are displayed in figure \ref{fig:fluxes}. In the following, they are compared to the expectations from section \ref{sec:fluxes}, taking into consideration the weather conditions displayed in figures \ref{fig:horspeed}, \ref{fig:radiation} and \ref{fig:RH}:\\
The ground flux is higher than expected (compare table \ref{tab:groundbowen} to equation \ref{eq:qgqs}).
A possible explanation are the neutral stability conditions: They imply less turbulence and therefore lower sensible and latent heat fluxes as compared to unstable conditions, which are more common at daytime.\\
The Bowen ratio $\frac{Q_H}{Q_E}$ is lower than expected, especially from 2:30-4 (compare table \ref{tab:groundbowen} to equation \ref{eq:bowen}). This may be due to the rain, which made the ground colder and wetter.\\
$Q_H$ is lower from 2:30-4 than from 2-2:30 - assumingly because of the rain event, which cooled the ground.
During the rain event, $Q_H$ was probably negative, balancing positive contributions from the non-rainy parts of the intervals 2:30-3 and 3-3:30.
It is assumed that after the rain event, the sun slowly warmed the ground again and therefore $Q_H$ slowly rose again in the last interval.\\
$Q_E$ increased slightly from the first to the second interval, although $Q_S$ decreased.
This is probably because the shower made the ground wetter.
In the third interval, $Q_E$ decreases to \SI{48}{\percent} of the previous value.
This is probably due to higher relative humidity, lower wind speed and less sun.
Surprisingly, in the last interval, $Q_E$ becomes larger than before the rain event, even though there is less sun and less wind.
This can be because the ground is wetter and the air is dryer.
