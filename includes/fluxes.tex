\subsubsection{Heat Fluxes}\label{sec:fluxes}
Through an infinitely thin layer % (i.e. a plane) 
between ground and air (in the following called surface), there are the following heat fluxes:
\begin{itemize}
	\item Sensible heat flux $Q_H$,
	\item latent heat flux $Q_E$,
	\item ground flux $Q_G$, i.e. the heat flux that cools or heats the ground, and
	\item radiation $Q_S=K\uparrow+K\downarrow+I\uparrow+I\downarrow$,\\
	with $K\uparrow$ and $K\downarrow$ the outgoing and incoming shortwave radiation,\\
	and $I\uparrow$ and $I\downarrow$ the outgoing and incoming longwave radiation.
\end{itemize}
%
Here positive values mean heat fluxes that take energy out of the surface.
This implies that for the fluxes in the atmosphere ($Q_H$, $Q_E$ and $Q_S$), positive values mean upward fluxes heating the atmosphere.
For the ground flux, positive values mean downward fluxes, heating the ground.
\\
As an infinitely thin layer cannot store energy, these four fluxes through the surface add up to zero:
\begin{equation}
Q_S+Q_H+Q_E+Q_G=0.
\end{equation}
According to \cite[p. 255]{Stull}, \enquote{[T]he neglect of the storage layer works well for quasi-steady-state situations where there is no appreciable change in the mean temperature of that layer.
It also works well for flat barren land surfaces}.
Neither is the case in this experiment:
Sunny, cloudy and rainy moments took turns during the measurements, which were carried out over grass, not a barren surface.
Therefore it has to be assumed that the layer between soil and top of the grass (the \enquote{grass layer}) did store energy and change temperature during the measurements.\\
In this experiment, the top of the grass is taken as the surface.
The ground flux is calculated as the negative sum of the other heat fluxes:
\begin{equation}
Q_G=-(Q_S+Q_H+Q_E). \label{eq:groundflux}
\end{equation}
The result is the total heat flux from the surface downwards.
As the grass layer can store energy, the ground flux calculated that way differs from the actual ground flux.
\\
In this experiment, the radiation is measured at the upper arm of the met mast.
The radiation at the surface can be assumed to be the same, because the emission and absorption of the \SI{5.875\pm 0.040}{\metre} of air between the surface and the upper arm is negligible.\\
Sensible and latent heat flux are measured by the IRGASONs at the upper and lower arm.
As the met mast lies within the Prandtl layer, the sensible and latent heat flux at the surface can be assumed to be the same (or up to \SI{10}{\percent} higher).
It is therefore also expected that both IRGASONs measure the same fluxes, or that the upper one measures a less than \SI{10}{\percent} lower value than the lower one.
\\
In the following, it is discussed what values of the different fluxes are expected to be measured.
\\
The incoming short wave radiation (sunlight) in sunny moments can be modelled as 
\begin{equation}
K\downarrow = S(0.6+0.2\sin(\Psi))\sin(\Psi) \qquad \text{\cite[p. 257]{Stull}},
\end{equation}
with the solar irradiance $S=\SI{-1370}{\watt\per\metre^2}$ and the solar elevation angle $\Psi$, which is calculated according to fomulas 7.3.1c and 7.3.1d in \cite[p. 257f]{Stull}. The result can be seen in figure \ref{fig:radiation}. In cloudy moments it is considerably less.
\\
The outgoing short wave radiation is the reflected and scattered sunlight and therefore only a fraction of the incoming one, in the case of grass roughly \SI{20}{\percent}:
\begin{equation}
K\uparrow=-0.2K\downarrow \quad \text{\cite[p.~258]{Stull}.}
\label{eq:albedo}
\end{equation}
\\
The outgoing long wave radiation, i.e. the earth's black body radiation, can be calculated as
\begin{equation}
I\uparrow=\epsilon_{IR}\,\sigma_{SB}\,T^4 \qquad \text{\cite[p.~259]{Stull}},
\end{equation}
with the Stefan-Boltzmann constant $\sigma_{SB}=\SI{5.67e-8}{\watt\per(\meter^2\kelvin^4)}$, the infrared emissivity $\epsilon_{IR}$ and the ground temperature $T$.
For most surfaces, $\epsilon_{IR}=\SI{0.95\pm0.05}{}$.
The air temperature is between \SI{13}{\degreeCelsius} and \SI{15}{\degreeCelsius} in this experiment.
The ground temperature can be higher or lower than the air temperature, therefore $T=\SI{287\pm5}{\kelvin}$ is estimated for the ground temperature.
This yields
\begin{equation}
I\uparrow=\SI{365\pm45}{\watt\per\metre^2}.
\label{eq:blackbody}
\end{equation}
As the ground temperature changes slowly, so does $I\uparrow$.
\\
The incoming long wave radiation is the part that is reflected and scattered from the clouds in the atmosphere.
Therefore, it is smaller than the outgoing one: $-I\downarrow<I\uparrow$.
It highly depends on the cloud cover.
According to \cite[p.~258]{Stull}, for overcast clouds at high, middle and low level, it approximately equals the outgoing one:
$-I\downarrow \approx I\uparrow$.
\\
In a rough approximation, the ground flux is \SI{10\pm 5}{\percent} of the net radiation:
\begin{equation}
{Q_G=-0.1Q_S} \quad \text{\cite[p. 282]{Stull}.}
\label{eq:qgqs}
\end{equation}
\\
Sensible heat flux is due to vertical movement of air and vertical temperature gradients.
Latent heat flux is due to vertical movement of air and vertical absolute humidity gradients.
Sensible and latent heat flux are correlated: Vertical temperature gradients produce sensible heat fluxes.
But the absolute humidity is generally higher in air parcels with higher temperature.
Therefore, temperature gradients correlate with absolute humidity gradients, which in turn produce latent heat fluxes.
\\
The sensible heat flux is generally positive during the day (at least in the afternoon) because the ground is warmer than the air due to the solar radiation.
It is higher during and after sunny moments.
It becomes small or even negative during and after rain events because the rain cools the ground more than the air.
\\
The latent heat flux is generally positive during the day (at least in the afternoon).
It is higher when it is windy or sunny or the ground is wet or warm or the air is dry.
\\
According to \cite[p. 274]{Stull}, the \textit{Bowen ratio} of sensible to latent heat flux is over grasslands roughly
\begin{equation}
	\frac{Q_H}{Q_E}=0.5.
	\label{eq:bowen}
\end{equation}