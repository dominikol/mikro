In this section, it is assessed how large the errors of the desired quantities are.

\subsubsection{Weather and Humans}
It is assumed that the humans' interventions mentioned in section \ref{sec:weather} have a low impact:\\
The IRGASON's protective caps as well as people moving sometimes close to the met mast influence the aerodynamics slightly.\\
Touching an IRGASON can produce a spike in the measurements, which is possibly filtered out by TK3's despiking method.
If not, it is still a short event, which changes a \SI{30}{\minute} mean little.
\\\\
Shaking of the met mast implies rotation and horizontal and vertical translation of the IRGASONs. 
These movements are nearly periodic with zero mean velocity.
The rotation of the IRGASONs means that a part of the actually horizontal wind velocity is detected as vertical wind velocity.
This does not change the mean vertical velocity, but increases its fluctuation, and therefore the vertical fluxes.
It is difficult to estimate by how much this increases the vertical fluxes.
But much more for Caurus than for Mistral, because the amplitude of the shaking increases more than linearly with height.
This is another reason to use Mistral's measurements for the surface fluxes, not Caurus'.\\
The met mast shook only when it was windier.
Therefore, it is assumed that it effects only the fluxes between 2 and 3 p.m., which is when it was windier (see figure \ref{fig:horspeed}).
\\\\
It can be expected that during the rain event rain drops on the gas analyzer lower its signal strength and therefore disturb the measurements.
But this is not the case according to the diagnostic flags that the IRGASONs give out and that include the signal strength as a criterion.
For both Mistral and Caurus, both the sonic and gas analyzer flags are $0$ at nearly all times between 2 and 4, which means that no problems were detected \cite{irgason}.
The only exception is that Caurus' sonic flag is $4$ at $6$ out of \SI{144000}{} sampling points.
But the measured values at these points are close to the adjacent ones.
Therefore, these non-zero flag values can be neglected.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{img/winddir}
	\caption[Histogram of wind directions]{Histogram of wind directions in compass coordinate system measured by Caurus from 2-2:30.}
	\label{fig:winddir}
\end{figure}

\subsubsection{Eddy-Covariance Assumptions}
\label{sec:errforest}
In the following, it is evaluated whether the assumptions of the eddy-covariance method (see section \ref{sec:eddycov}) are fulfilled in this experiment.
\paragraph{Stationarity}
To monitor the data quality, TK3 performs a steady state test (which evaluates whether the assumption of stationarity is fulfilled) and a test on fully developed turbulence.
The results are summed up in quality flags for the quantities $Q_H$ and $Q_E$, among others. \cite{tk3}\\
\enquote{For practical application the quality flags can be interpreted in the following way:\\
	Class 0: high quality data, use in fundamental research possible\\
	Class 1: moderate quality data, no restrictions for use in long term observation programs} \cite[p. 35]{tk3}\\
In this case, all the flags are $0$ apart from Caurus' flag for $Q_H$ in the third time interval and Mistral's flag for $Q_E$ in the fourth time interval, which are $1$.
This is surprising because the weather was not stationary (see section \ref{sec:weather}).
\paragraph{Flat and homogeneous terrain}
In figure \ref{fig:gelaendeskizze} it can be seen that the wind comes on average from the south-eastern corner of the forest.
Figure \ref{fig:winddir} shows that the wind direction is rather variable.
But the direction of an air parcel does not stay constant over \SI{100}{\meter}.
Therefore, from the wind direction at one moment, one cannot tell where that air parcel was before \SI{100}{\meter}.
But it can be assumed that the wind comes sometimes from the forest and sometimes from the field south of the forest.
Being directly next to the forest, the street can be neglected with regard to its influence on the flow.
So flat and homogeneous terrain is given upwind of the met mast up to the forest, which is at a distance of $x_m=\SI{115\pm5}{\meter}$ according to section \ref{sec:terrain}.
Then, with the forest, the displacement distance and surface roughness change.
%Sections \ref{sec:heightIBL} and \ref{sec:errforest}
The two following paragraphs evaluate whether this distance from the forest is sufficient.