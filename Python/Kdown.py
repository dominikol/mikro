# Stull Formeln 7.3.1a-d, S.257f
# Ergebnisse in radtheo.dat

from math import pi,cos,sin

S=1370.0
phi=52.1994*2*pi/360
l=-8.1114*2*pi/360
#t=13.0
phir=0.409
d=260.0
dr=173.0
dy=365.25

delta=phir*cos(2*pi*(d-dr)/dy) # Formel 7.3.1d
z=1.0/12.0 # 5 min in Stunden

for i in range(-2,26):
	t=12.0+i*z # Zeiten von 11.50-14.05 in UTC
	sinpsi=sin(phi)*sin(delta)-cos(phi)*cos(delta)*cos((pi*t/12)-l) # Formel 7.3.1c
	K=S*sinpsi*(0.6+0.2*sinpsi) # Formeln 7.3.1a+b
	print(K)
