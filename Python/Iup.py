# Stull Formel 7.3.2b S.259

e=0.95
ee=0.05

s=5.67e-8

T=287.0
Te=5.0

I=e*s*T**4
Ie=I*(ee/e+4*Te/T)

print(I)
print(Ie)
