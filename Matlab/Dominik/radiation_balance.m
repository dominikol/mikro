clear;
close all;
clc;
addpath(genpath('/home/dominik/Documents/MATLAB'));
latexpath='/home/dominik/Documents/Uni/SS19/FPRM/Mikrometeorologie/Protokoll2/img/';
%%
load('data/Radiation');
% die theoretischen Werte hab ich in Python berechnet, siehe Protokoll /
% Stull
% keine NaNs
%% alle Strahlungen
close all;
figure('Units','centimeters','Position',[10, 10, 15, 12],...
        'Name','Radiation','NumberTitle','off');
plot(time,rad,timetheo,radtheo);
legend('$-K\downarrow$','$K\uparrow$','$-I\downarrow$','$I\uparrow$',...
    '$-K\downarrow _{theo}$','interpreter','latex');
ylabel('radiation in W/m$^2$','interpreter','latex');
grid on;
xlim([time(1) time(1564)]);
xticklabels({'2:00','2:30','3:00','3:30','4:00'});
xlabel('time');

%export_fig(strcat(latexpath,'radiation'),'-eps','-transparent');
%% shortwave und albedo
albedo=rad(:,2)./rad(:,1);
figure;
yyaxis left
plot(time,rad(:,1:2));
yyaxis right
plot(time,albedo);
legend('$K\downarrow$','$K\uparrow$','albedo','interpreter','latex');
min(albedo)
max(albedo)
%% Hier hab ich die Zeitvektoren dazugemacht und dann mit abgespeichert.
% start=datetime(2019,9,17,13,49,45);%CEST
% endtime=datetime(2019,9,17,16,5,0);
% time=linspace(start,endtime,length(rad));
% starttheo=datetime(2019,9,17,13,50,0);%CEST
% endtheo=datetime(2019,9,17,16,5,0);
% timetheo=linspace(starttheo,endtheo,length(radtheo));