%%
clear;
close all;
clc;
addpath(genpath('/home/dominik/Documents/MATLAB'));
latexpath='/home/dominik/Documents/Uni/SS19/FPRM/Mikrometeorologie/Protokoll2/img/';
load('data/Mistral_Slow_IRG');
load('data/mcolors');
%save('data/Mistral_Slow_IRG');
%% Hier hab ich den Zeitvektor dazugemacht und dann mit abgespeichert.
% start=datetime(2019,9,17,13,44,55);%CEST
% endtime=datetime(2019,9,17,16,4,50); % 5,20,650
% time=linspace(start,endtime,length(Tc));
%%
idx=182:181+1440;
rainstart=time(182+540);
rainend=time(182+840);
subt=2;
add=9;
close;
figure('Units','centimeters','Position',[10, 10, 15, 12],...
        'Name','RH','NumberTitle','off','Color',[1,1,1]);
yyaxis left
plot(time(idx), RH(idx));%,'LineWidth',2
hold on;
ylabel('relative humidity / %');
ylim([55-subt 95+add]);
yyaxis right
ax=gca;
ax.YColor=[0 0 0];
plot(time(idx), H2O_mixratio(idx),'Color',mcolors(2,:),'LineStyle','--');
hold on;
plot(time(idx), Tc(idx),'Color',mcolors(3,:),'LineStyle','-');%
plot([rainstart rainstart],[0 100],'k-',[rainend rainend],[0 100],'k-');
ylim([10-subt/5 18+add/5]);
%ylabel('H2O mixing ratio / mmol/mol (bottom) and T / °C (top)');

legend('relative humidity / % (left y-axis)',...
    'H2O mixing ratio / mmol/mol (right y-axis)','T / °C (right y-axis)',...
    'start and end of rain');
xticklabels({'2:00','2:30','3:00','3:30','4:00'});
xlabel('time');
title('Mistral');
grid on;
%export_fig(strcat(latexpath,'RH'),'-eps','-transparent');
%%
% yyaxis right
% plot(time,H2O_density);
% legend('sonic','abs');
% %%
% close;
% plot(time, Tc);
% %%
% close;
% plot(time,Tc-amb_tmpr);
% %%
% mean(Tc-amb_tmpr);
% mean(RH-amb_RH);