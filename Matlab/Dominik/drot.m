function [a,b,u2] = drot(u0,v0,w0)
% Berechne die Winkel der Koordinatenrotation nach Skript S.29-31
a=atan(v0/u0); % alpha

u1=cos(a)*u0+sin(a)*v0;
v1=-sin(a)*u0+cos(a)*v0;
w1=w0;

b=atan(w1/u1); % beta
u2=cos(b)*u1+sin(b)*w1;
w2=-sin(b)*u1+cos(b)*w1;
% in Grad umwandeln
b=b*180/pi;
a=a*180/pi;
end

