% Mistral
clc;
close all;
clear;
latexpath='/home/dominik/Documents/Uni/SS19/FPRM/Mikrometeorologie/Protokoll2/img/';
addpath(genpath('/home/dominik/Documents/MATLAB'));
load('data/Radiation');
load('../Max/TK3_Ergebnisse.mat');
%% mean net radiation
netrad=rad(:,1)-rad(:,2)+rad(:,3)-rad(:,4);
radmean=zeros(4,1);
start=124;
step=360;
for i=1:4
    radmean(i)=mean(netrad(start+step*(i-1):start+step*i-1));
end
gflux=radmean-Mistral_values(:,38)-Mistral_values(:,40);
%%
close;
figure('Units','centimeters','Position',[10, 10, 15, 12],...
        'Name','fluxes Mistral','NumberTitle','off','Color',[1,1,1]);
bar([radmean Mistral_values(:,38) Mistral_values(:,40) gflux]);
ylim([-10 300]);
legend('-Q_S','Q_H','Q_E','Q_G');
%title('Mistral');
ylabel('heat fluxes in W/m$^2$','interpreter','latex');
    xlabel('time interval');
    xticks([1 2 3 4]);
    xticklabels({'2-2:30','2:30-3','3-3:30','3:30-4'});

grid on;
%export_fig(strcat(latexpath,'fluxes_Mistral'),'-eps');
%%
gflux./radmean;
Mistral_values(:,38)./Mistral_values(:,40)