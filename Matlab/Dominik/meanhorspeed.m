function [means] = meanhorspeed(u0,v0,w0,a,b)
%s
u1=cos(a)*u0+sin(a)*v0;
v1=-sin(a)*u0+cos(a)*v0;
w1=w0;

u2=cos(b)*u1+sin(b)*w1;
v2=v1;
w2=-sin(b)*u1+cos(b)*w1;

s=sqrt(u2.*u2+v2.*v2);
means=mean(s);
end

