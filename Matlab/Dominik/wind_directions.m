% für Caurus, als Kommmentar dahinter für Mistral

clear;
close all;
clc;
addpath(genpath('/home/dominik/Documents/MATLAB'));
latexpath='/home/dominik/Documents/Uni/SS19/FPRM/Mikrometeorologie/Protokoll2/img/';

load('data/Caurus_windspeeds'); % Mistral
%%
start=22322; % index von 14 Uhr: 18113
%f=20 Hz, d.h. 1/2 h = 20*60*30=36000 samples:
step=36000;
idx=start:start+4*step;
a=267.5-180/pi*atan(v(idx)./u(idx));%
%%
%plot(time(idx),a);
%%
close;
figure('Units','centimeters','Position',[10, 10, 15, 10],...
        'Name','Radiation','NumberTitle','off','Color',[1,1,1]);
histogram(a(1:step));
title('Caurus 2-2:30 p.m.');
xlabel('$\varphi$ / $^\circ$','interpreter','latex');
ylabel('counts');
grid on;
export_fig(strcat(latexpath,'winddir'),'-eps');