clear;
close all;
clc;
%%
hold on;
for i=1:7
    plot([0 1],[i i]);
end
legend('1','2','3','4','5','6','7');
ax=gca;
ch=ax.Children;
mcolors=zeros(7,3);
for i=1:7
    mcolors(8-i,:)=ch(i).Color;
end
save('data/mcolors','mcolors');