% für Caurus, als Kommmentar dahinter für Mistral

clear;
close all;
clc;
addpath(genpath('/home/dominik/Documents/MATLAB'));
load('data/Caurus_windspeeds'); % Mistral
% keine NaNs
%% Hier hab ich den Zeitvektor dazugemacht und dann mit abgespeichert.
% start=datetime(2019,9,17,13,41,20,50);%CEST 44,54,400 
% endtime=datetime(2019,9,17,16,5,3,450); % 5,20,650
% time=linspace(start,endtime,length(u));
% time.Format='yyyy-MM-dd HH:mm:ss.SSS';
%% Drehwinkel berechnen
% Drehwinkel für die gesamte Messdauer:
[amean,bmean,u2mean]=drot(mean(u),mean(v),mean(w));
start=22322; % index von 14 Uhr: 18113
%f=20 Hz, d.h. 1/2 h = 20*60*30=36000 samples:
step=36000;
% Drehwinkel für jede der halben Stunden berechnen
% Variablen initialisieren:
b=zeros(4,1);
a=b;
u2=b;
meanhorcau=b;
for i=1:4
    idx=start+(i-1)*step:start+i*step-1; % Index-range der i-ten halben Stunde
    [a(i),b(i),u2(i)]=drot(mean(u(idx)),mean(v(idx)),mean(w(idx)));
    meanhorcau(i)=meanhorspeed(u(idx),v(idx),w(idx),a(i)*pi/180,b(i)*pi/180);
end
idx=start:start+4*step-1;
[a2,~,~]=drot(mean(u(idx)),mean(v(idx)),mean(w(idx)));
267.5-a;
267.5-a2;
%% mean wind speed
% u2mean: magnitude of mean velocity
% mspeed= mean magnitude of velocity
speed=zeros(size(u));
for i=1:length(u)
    speed(i)=sqrt(u(i)^2+v(i)^2+w(i)^2);
end
mspeed=mean(speed);
