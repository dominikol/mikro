% kleine Änderungen von Dominik 4.4.20
close all; clc; clear;
addpath(genpath('/home/dominik/Documents/MATLAB'));

load('TK3_Ergebnisse.mat');

%latexpath='/Users/maxbodenstein/Documents/Universit�t/FPR-M/Mikrometeorologie/bit_Protokoll_Mikro/img/';
latexpath='/home/dominik/Documents/Uni/SS19/FPRM/Mikrometeorologie/Protokoll2/img/';

%% Name Plot:
names = strings(1,length(Mistral_header)); 
names(3) = 'horspeed';
names(10) = 'T';
names(22) = 'Covuw';
names(38) = 'Q_H';
names(40) = 'Q_E';
% ylabel:
Mistral_header(3)='{\its} / m/s';
Mistral_header(22)='{\it-\tau_z_x} / m^2/s^2';
Mistral_header(38)='{\itQ_H} / W/m^2';
Mistral_header(40)='{\itQ_E} / W/m^2';
%% plot results

save_plot = true;       % want to save plot?
%save_plot = false;

for i=[3 22 38 40]                   %[3 6 8 9 20:22 25 31 34 36 37 38 40]%41%13:15 %? AN AL AO AX
    figure('Units','centimeters','Position',[10, 10, 15, 12.5],'Name',Mistral_header(i),'Color',[1,1,1]);
    if i==22
        Caurus_values(:,i)=-Caurus_values(:,i);
        Mistral_values(:,i)=-Mistral_values(:,i);
    end
    if i==3
        load('../Dominik/data/meanhorspeed');
        Caurus_values(:,i)=meanhorcau;
        Mistral_values(:,i)=meanhormis;
    end
    % bar Plot
    % Untere Grenze von bar plot
    bar_min = round(min(min(Caurus_values(:,i),Mistral_values(:,i))) - ...
        0.2 * abs(min(min(Caurus_values(:,i),Mistral_values(:,i)))),1);
    if i==22
        bar_min=0;
    end
     % Obere Grenze von bar plot
    maxval=max(max(Caurus_values(:,i),Mistral_values(:,i)));
    bar_max = round(maxval*1.025,1);
% das löst Dominik jetzt anders:
%     if maxval<0
%         bar_max = ceil(maxval*0.975);
%     end
    hold on;
    bar(Caurus_values(:,i),0.35,'Facecolor',[1,0,0]); 
    bar(Mistral_values(:,i),0.5,'Facecolor',[0,0,1],'Edgecolor',[0,0,1]); 
    bar(Caurus_values(:,i),0.35,'Facecolor',[1,0,0],'FaceAlpha',.0,'EdgeColor',[1,0,0])
    grid minor;
    hold off;
    
    % normaler Plot mit Datenpunken:
    %plot(Caurus_values(:,i),'o','linewidth',1.5);  hold on;
    %plot(Mistral_values(:,i),'o','linewidth',1.5); hold off;
    
    if i==40
        legend('Caurus','Mistral','Location','north');
    else
        legend('Caurus','Mistral');
    end
    ylabel(Mistral_header(i));
    xlabel('time interval');
    xticks([1 2 3 4])
    xticklabels({'2-2:30','2:30-3','3-3:30','3:30-4'});
    yticks('auto')
    %yticks([round(linspace(bar_min, bar_max, 10),2)])
    axis([0.5 4.5 bar_min bar_max])
    box on;
    
    if save_plot && (names(i) ~= "")
        export_fig(strcat(latexpath,names(i)),'-eps');%,'-transparent'
        %close all;
    elseif names(i) == "" && save_plot
        fprintf('Erst Namen f�r plot angeben, siehe eine Section weiter oben')            
    end  % saves plot and controlls if plot_name is given
    
end


